# Preliminary Simplified Preference Allocator

import re
from pulp import *
from random import randint


# configuration
SEMESTER   = 4
MAX_REPEAT = 2
TIMEOUT    = 1000
SIMULATE   = True


def getField(name, min_=None, max_=None):
    if name in Field.lookup:
        return Field.lookup[name]
    assert None not in (min_, max_)
    return Field(name, min_, max_)


class Field:
    lookup = {}
    def __init__(self, name, min_, max_):
        assert min_ <= max_
        assert name not in Field.lookup

        self.name = name
        self.min_ = min_
        self.max_ = max_
        Field.lookup[name] = self


class Preference:
    def __init__(self, yes, no):
        assert not set(yes).intersection(no)

        self.yes = [getField(f) for f in yes]
        self.no = [getField(f) for f in no]

    def preferBy(self, field):
        if field in self.no:
            return -1
        if field not in self.yes:
            return 0
        return len(self.yes) - self.yes.index(field) - 1


def load_classes(argv):
    classes = []

    for arg in argv:
        try:
            name, min_, max_ = arg.split(':')
            classes.append(getField(name, int(min_), int(max_)))
        except ValueError:
            pass

    return classes


def simulate_classes(count, cls):
    a = int(count/len(cls))
    for c in cls:
        min_ = randint(int(a-a*0.1), a)
        max_ = randint(a, int(1+a+a*0.2))
        getField(c, min_, max_)


def load_prefs(lines, simulate=False):
    cls   = set()
    prefs = []
    yes   = None
    no    = None
    pat   = re.compile(r'^(yes|no): *(.*)$')

    for line in lines:
        m = pat.match(line)
        if m:
            if m[1] == "yes":
                yes = m[2].split()
                cls |= set(yes)
            elif m[1] == "no":
                no = m[2].split()
                cls |= set(no)
        elif yes:
            prefs.append((yes, no))
            yes, no = None, None
    if yes:
        prefs.append((yes, no))

    if simulate:
        simulate_classes(len(prefs), cls)

    return [ Preference(*p) for p in prefs]


def construct(classes, prefs):
    p = LpProblem("swp", LpMaximize)

    # reverse ids
    rid = { cls:k for k, cls in enumerate(classes) }

    # preferences x semester matrix
    mat = [[[LpVariable("p{}_s{}_c{}".format(i+1, j+1, k),
                       0, 1, LpBinary)
             for k in range(len(classes))]
            for j in range(SEMESTER)]
           for i in range(len(prefs))]

    # OBJECTIVE: Bevorzugte sportkurse zuweisen
    p += lpSum([mat[i][j][k] * pref.preferBy(cls)
                for k, cls in enumerate(classes)
                for j in range(SEMESTER)
                for i, pref in enumerate(prefs)
                if pref.preferBy(cls) >= 0])

    # CONSTRAINT 0: in jedem semester wird *ein* sportfach zugewiesen
    for sem in mat:
        for cls in sem:
            p += sum(cls) == 1

    # CONSTRAINT 1: ein sportfach nicht mehr als zwei mal belegen
    for sem in mat:
        for k in range(len(classes)):
            p += sum(v[k] for v in sem) <= MAX_REPEAT

    # CONSTRAINT 2: ein sportfach soll im semester nicht ueber/unterbelegt werden
    for j in range(SEMESTER):
        for k, c in enumerate(classes):
            p += c.min_ <= sum(p[j][k] for p in mat) <= c.max_
            # p += sum(p[j][k] for p in mat)

    # CONSTRAINT 3: ausgeschlossene sportkurse sollen nicht zugewiesen werden
    for i, pref in enumerate(prefs):
        for no in pref.no:
            k = rid[no]
            for sem in mat[i]:
                p += sem[k] == 0

    return p, mat


def show(prob, mat, classes, prefs):
    print("Status: ", prob.status)

    if prob.status != constants.LpStatusOptimal:
        return

    for i, pref in enumerate(prefs):
        print("= STUDENT {} (yes: {}) (no: {})".format(i,
                                                       [p.name for p in pref.yes],
                                                       [p.name for p in pref.no]))
        avg = 0
        for j in range(SEMESTER):
            k = next(k for k, v in enumerate(mat[i][j])
                     if v.value() == 1)
            cls = classes[k]
            place = len(pref.yes) - pref.preferBy(cls)
            avg += place
            print("{:2}: {} ({})".format(j+1, cls.name, place))
        print("average: {}".format(avg/SEMESTER))
        print()


# call procedure
if __name__ == "__main__":
    prefs      = load_prefs(iter(sys.stdin), simulate=True)
    # load_classes(sys.argv)
    classes    = list(Field.lookup.values())
    prob, mat  = construct(classes, prefs)
    prob.writeLP("optim.lp")
    prob.solve()
    show(prob, mat, classes, prefs)
